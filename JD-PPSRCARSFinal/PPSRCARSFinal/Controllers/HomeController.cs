﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PPSRCARSFinal.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult WhatWeOffer()
        {
            ViewBag.Message = "What We Offer";

            return View();
        }

        public ActionResult FAQ()
        {
            ViewBag.Message = "Have a question in your mind?";

            return View();
        }
        public ActionResult Blog()
        {
            ViewBag.Message = "Knowledgebase";

            return View();
        }
        public ActionResult ContactUs()
        {
            ViewBag.Message = "Contact Us";

            return View();
        }
        public ActionResult TC()
        {
            ViewBag.Message = "Terms And Conditions";

            return View();
        }
        public ActionResult Disclaimer()
        {
            ViewBag.Message = "Terms And Conditions";

            return View();
        }
        public ActionResult PrivacyCookies()
        {
            ViewBag.Message = "Terms And Conditions";

            return View();
        }
    }
}