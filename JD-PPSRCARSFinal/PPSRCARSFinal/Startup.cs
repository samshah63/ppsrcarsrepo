﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PPSRCARSFinal.Startup))]
namespace PPSRCARSFinal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
